# blug-2019-11-14

Docker-based R&D is fast AF

Building images from crappy Dockerfiles is bad.
Using official images while you learn how to write good Dockerfiles is less bad.

In fact, it's good. Because it gives you access to a zillion "machines" written by very smart people. Often, people who are expert in the thing you want to deploy.

Study the content of the images. Study the Dockerfiles.

When you're confident that you can write non-crap Dockerfiles. Do that.

Go back and un-crap crappy Dockerfiles that you and others have already written.

## Exercise 1: Testing a shell script for portability.

Say you're one of the lead teachers in a Software Freedom School. You'd like to make a script that downloads class materials from git into a known directory structure, so that you can offer specific instructions like:

```bash
cd
cd sfs/pipeline-play
less README.md
```

And, you can trust that they'll work consistently on all students' machines.

The class downloader needs to run on all the Linux. So, let's test it on enough of them that we're confident it will.

```bash
docker run -it ubuntu /bin/bash
apt update && apt -y install wget
wget https://www.sofree.us/bits/git-a-class
chmod +x git-a-class
./git-a-class pipeline-play
```

```bash
docker run -it fedora /bin/bash
curl -O https://www.sofree.us/bits/git-a-class
chmod +x git-a-class
./git-a-class pipeline-play
```

```bash
docker run -it opensuse/leap /bin/bash
zypper install -y curl
curl -O https://www.sofree.us/bits/git-a-class
chmod +x git-a-class
./git-a-class pipeline-play
```

That's probably enough for now. The nice thing about this way of testing for portability across distributions is that it only takes a few minutes per distro, where doing the same thing in vagrant takes *much* longer.

## Exercise 2: Web servers one two

Let's say you want a simple web-server, nothing in particular, just something that returns something, so you can test a firewall, or a load-balancer, or whatever.

```bash
docker run --name testy --detach httpd
docker inspect testy | grep IPA
```

Now, `curl`, `xdg-open`, or manually point your web-browser at that IP. And it works just the same with nginx...

```bash
docker rm -f testy
docker run --name testy --detach nginx
docker inspect testy | grep IPA
```

But, really, you're trying to help your friend/relative create a *pretty* web-site. And, web-dev is *hard*. And, one only has so much time available. Maybe this is a job for Wordpress? Let's give that a swing...

```bash
docker network create wordpress-net
docker run --detach --network wordpress-net \
    -e MYSQL_RANDOM_ROOT_PASSWORD=true \
    --name wordpress-db \
    mariadb
docker run --detach --network wordpress-net \
    --name wordpress-app \
    wordpress

docker inspect wordpress-app | grep IPA

docker logs wordpress-db 2>&1 | grep ROOT
# if that didn't return anything, wait, like this:
until docker logs wordpress-db 2>&1 | grep ROOT
do
  echo -n .
  sleep 1
done
```

Use `xdg-open` or manually direct your browser to wordpress-app's IP Address and connect the -app to the -db. Note that putting the machines on a shared network let them address one another by name.

[example first config screen](wordpress-config-example.png)

# Exercise 3: Prometheus and friends

Suppose that you want to learn to use Prometheus. You quickly find that other things are needed, like Grafana. You do a little looking up, so you can separate important config from not-so-important data. You write a nice little script to yoink the stock config from fresh containers: `pave`. You write a nice little script to blast away the things you've yoinked: `nuke`. You discard a *lot* of manual startup scripts, realizing that `docker-compose` is a fresh and flowery way to accomplish the same things you've been doing with them.

```bash
git clone https://gitlab.com/DLWillson/blug-2019-11-14.git
cd blug-2019-11-14/monitoring/
./pave
docker-compose up
./browse
```

In the browser, connect Grafana to Prometheus.

- default Grafana login is admin:admin
- add a Prometheus data-source. HTTP URL is http://Prometheus:9090
  Thank goodness for free name-resolution on a dedicated network, right?
- do more clever things here as you learn Grafana and Prometheus (and Alertmanager and Blackbox exporter)

# Exercise 4: Rainbow (WIP)

WIP in rainbow/

# Exercise 7: Nethack

You've worked hard. Play a little!

```bash
docker run -it matsuu/nethack
```

Or, if you prefer to suffer some more...

https://github.com/bencawkwell/dockerfile-dwarffortress
