#!/bin/bash -u
for color in red orange yellow green blue purple
do
	echo $color
    docker rm --force $color
done

docker rm --force rainbow

docker network rm rainbow-net
